/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giovannicandido.simulacao.forms;

import com.giovannicandido.simulacaoforms.form.Item.ItemEmail;
import com.giovannicandido.simulacaoforms.form.Item.ItemFormulario;
import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author giovanni
 */
public class TestItem {
    /**
     * Teste para validacao de itemEmail
     */
    @Test
    public void testItemEmail(){
        ItemFormulario item = new ItemEmail();
        String emailValido = "giovanni@pucminas.br";
        String emailInvalido = "gio./spuc.com";
        item.setValor(emailValido);
        Assert.assertTrue(item.isValid());
        item.setValor(emailInvalido);
        Assert.assertFalse(item.isValid());
        emailInvalido = "gio@com";
        item.setValor(emailInvalido);
        Assert.assertFalse(item.isValid());
    }
}
