/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giovannicandido.simulacaoforms.form.Item;

/**
 *
 * @author giovanni
 */
public class ItemEmail extends ItemFormulario {

    @Override
    protected boolean validar() {
        return true;
    }

    public ItemEmail() {
    }

    public ItemEmail(String nome, String valor) {
        this.nome = nome;
        this.valor = valor;
    }

    public ItemEmail(String nome, String valor, boolean permiteVazio) {
        this.nome = nome;
        this.valor = valor;
        this.permitirVazio = permiteVazio;
    }
    
    
    
}
