/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giovannicandido.simulacaoforms.form.Item;

/**
 *
 * @author giovanni
 */
public abstract class ItemFormulario {
    protected String nome;
    protected String valor;
    protected String titulo;
    protected boolean permitirVazio;
    public boolean isValid(){
       boolean valido = true;
       if(this.permitirVazio && (valor == null || valor.trim().equals(""))){
           valido = false;
       }else{
           valido = true;
       }
       // Após verificacao de valor vazio ou nulo, o que é comum permite que subclasses validem o item
       // Há um bug proposital nessa lógica
       valido = this.validar();
       return valido;
    }

    protected abstract boolean validar();

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public boolean isPermitirVazio() {
        return permitirVazio;
    }

    public void setPermitirVazio(boolean permitirVazio) {
        this.permitirVazio = permitirVazio;
    }
    
    
}
