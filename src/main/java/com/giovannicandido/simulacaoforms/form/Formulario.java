/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.giovannicandido.simulacaoforms.form;

import com.giovannicandido.simulacaoforms.form.Item.ItemFormulario;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author giovanni
 */
public class Formulario {
   private List<ItemFormulario> itens;
   private boolean enviado = false;

    public Formulario() {
        this.itens = new ArrayList<>();
    }
   public void addItem(ItemFormulario item){
       itens.add(item);
   }
   public void removeItem(ItemFormulario item){
       itens.remove(item);
   }
   public Iterator<ItemFormulario> getItems(){
       return itens.iterator();
   }
   /**
    * Valida formulario
    * @return 
    */
   public boolean isValid(){
       boolean valido = true;
       for(ItemFormulario item : itens){
           if(item.isValid() == false){
               valido = false;
               break;
           }
       }
       return valido;
   }
   /**
    * Simula o envio de formulario
    */
   public void submit(){
       if(isValid()){
          enviado = true; 
       }else{
           enviado = false;
       }
   }
   /**
    * Verifica se formulario foi enviado
    * @return 
    */
   public boolean foiEnviado(){
       return enviado;
   }
}
