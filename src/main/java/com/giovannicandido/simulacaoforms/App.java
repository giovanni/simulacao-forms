package com.giovannicandido.simulacaoforms;

import com.giovannicandido.simulacaoforms.form.Formulario;
import com.giovannicandido.simulacaoforms.form.Item.ItemEmail;
import com.giovannicandido.simulacaoforms.form.Item.ItemFormulario;
import java.util.Iterator;

/**
 * Hello world!
 *
 */
public class App implements Runnable
{
    public static void main( String[] args )
    {
        System.out.println( "#################Simulação de Formulários##############" );
        System.out.println("");
        new App().run();
        
    }
    public Formulario criarFormulario(){
        Formulario form = new Formulario();
        form.addItem(new ItemEmail("email","giovanni@pucminas.br"));
        form.addItem(new ItemEmail("email","giovanni@pucminas.br", false));
        return form; 
    }

    @Override
    public void run() {
       Formulario form = criarFormulario();
       Iterator<ItemFormulario> items = form.getItems();
       while(items.hasNext()){
           ItemFormulario item = items.next();
           System.out.printf("Item: %s Valor: %s Permite Vazio? %b Válido? %b%n", 
                   item.getNome(), item.getValor(), item.isPermitirVazio(), 
                   item.isValid());
           
           System.out.println("+++++++++++++++++++++++++");
          
       }
        System.out.println("Formulário é válido: " + form.isValid());
    }
}
